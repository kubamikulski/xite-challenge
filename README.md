# xite-challenge

To generate test coverage report (in target/scala-2.12/scoverage-report/index.html):

```
sbt clean coverage test coverageReport
```

To run:

```
sbt run
```

By default, server is starting on http://localhost:8085/

Sample requests with curl:

```
curl -d '{"userName": "David", "email": "david@gmail.com", "age": 28, "gender": 1}' -H "Content-Type: application/json" -X POST http://localhost:8085/register
curl -d '{"userId": 8050489824086482909, "videoId": 8795975294698091971, "actionId": 1}' -H "Content-Type: application/json" -X POST http://localhost:8085/action
```