package xite

import akka.actor.ActorSystem
import akka.testkit.{ ImplicitSender, TestKit }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }
import xite.Model.{ Action, Recommendation, User }
import xite.UserRegistryActor.{ PerformAction, RegisterUser }

import scala.concurrent.duration._

class ActorSystemSpec extends TestKit(ActorSystem()) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val duration = 5.seconds

  "UserRegistryActor" should {

    "create user actors on valid registration attempt" in {
      val registry = system.actorOf(UserRegistryActor.props)
      registry ! RegisterUser(User("john", "john@xite.com", 25, 1))
      expectMsgPF(duration) { case Right(Recommendation(_, _)) => () }
    }

    "check if user exists on action" in {
      val registry = system.actorOf(UserRegistryActor.props)
      registry ! RegisterUser(User("john", "john@xite.com", 25, 1))
      val recommendation = expectMsgPF(duration) { case Right(r: Recommendation) => r }
      registry ! PerformAction(Action(-1, recommendation.videoId, 1))
      expectMsgPF(duration) { case Left(InvalidUserId) => () }
    }

    "check if video sent in action matches the one issued previously" in {
      val registry = system.actorOf(UserRegistryActor.props)
      registry ! RegisterUser(User("john", "john@xite.com", 25, 1))
      val recommendation = expectMsgPF(duration) { case Right(r: Recommendation) => r }
      registry ! PerformAction(Action(recommendation.userId, -1, 1))
      expectMsgPF(duration) { case Left(InvalidVideoId) => () }
    }

    "respond with new recommendation of correct action" in {
      val registry = system.actorOf(UserRegistryActor.props)
      registry ! RegisterUser(User("john", "john@xite.com", 25, 1))
      val recommendation = expectMsgPF(duration) { case Right(r: Recommendation) => r }
      registry ! PerformAction(Action(recommendation.userId, recommendation.videoId, 1))
      expectMsgPF(duration) { case Right(Recommendation(_, _)) => () }
    }

  }

}
