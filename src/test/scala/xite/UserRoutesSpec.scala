package xite

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }
import xite.Model.{ Action, Recommendation, User }

class UserRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
  with UserRoutes {

  override val userRegistryActor: ActorRef =
    system.actorOf(UserRegistryActor.props, "userRegistry")

  lazy val routes = userRoutes

  "UserRoutes" should {

    "validate user data on register attempt (POST /register)" in {
      def checkUser(user: User, expectedErrors: Seq[DomainValidation]) = {
        val request = Post("/register").withEntity(Marshal(user).to[MessageEntity].futureValue)
        request ~> routes ~> check {
          status should ===(StatusCodes.BadRequest)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[ErrorResponse].errors should contain theSameElementsAs expectedErrors.map(_.errorMessage)
        }
      }
      checkUser(User("mr. invalid", "valid@email.com", 2, 1), Seq(InvalidAge))
      checkUser(User("mr. invalid", "valid@email.com", 25, 5), Seq(InvalidGender))
      checkUser(User("mr. invalid", "not an email", 25, 1), Seq(InvalidEmail))
      checkUser(User("mr. invalid", "not an email", 2, 5), Seq(InvalidAge, InvalidEmail, InvalidGender))
    }

    "successfully register a valid user (POST /register)" in {
      val user = User("john", "john@xite.com", 25, 1)
      val request = Post("/register").withEntity(Marshal(user).to[MessageEntity].futureValue)
      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        noException should be thrownBy entityAs[Recommendation]
      }
    }

    "validate actionId (POST /action)" in {
      val action = Action(-1, -1, 5)
      val request = Post("/action").withEntity(Marshal(action).to[MessageEntity].futureValue)
      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[ErrorResponse].errors should ===(Seq(InvalidActionId.errorMessage))
      }
    }

    "validate if user exists on action (POST /action)" in {
      val action = Action(-1, -1, 1)
      val request = Post("/action").withEntity(Marshal(action).to[MessageEntity].futureValue)
      request ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[ErrorResponse].errors should ===(Seq(InvalidUserId.errorMessage))
      }
    }

  }
}
