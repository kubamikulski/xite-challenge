package xite

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import xite.Model.{ Action, User }
import xite.UserActor.{ GetInitialRecommendation, GetNextRecommendation }

object UserRegistryActor {
  sealed trait RegistryMessage
  final case class RegisterUser(user: User) extends RegistryMessage
  final case class PerformAction(action: Action) extends RegistryMessage
  def props: Props = Props[UserRegistryActor]
}

class UserRegistryActor extends Actor with ActorLogging {
  import UserRegistryActor._

  var users = Map.empty[Long, ActorRef]

  def receive: Receive = {
    case RegisterUser(user) =>
      val userId = Util.randomLongs.filterNot(users.contains).head //or we could just give them sequential ids...
      log.info(s"registering $user with id $userId")
      val userActor = context.actorOf(UserActor.props(user, userId))
      users += userId -> userActor
      userActor ! GetInitialRecommendation(sender)

    case PerformAction(action) =>
      users.get(action.userId) match {
        case Some(userActor) => userActor ! GetNextRecommendation(sender, action.videoId)
        case None => sender ! Left(InvalidUserId)
      }

    case m => log.warning(s"unhandled message: $m")
  }

}