package xite

import cats.data._
import cats.implicits._
import uk.gov.hmrc.emailaddress.EmailAddress
import xite.Model.{ Action, User }

sealed trait DomainValidation {
  def errorMessage: String
}

case object InvalidEmail extends DomainValidation {
  override def errorMessage: String = "email is not valid"
}

case object InvalidAge extends DomainValidation {
  override def errorMessage: String = "age is not valid"
}

case object InvalidGender extends DomainValidation {
  override def errorMessage: String = "gender is not valid"
}

case object InvalidVideoId extends DomainValidation {
  override def errorMessage: String = "video does not correspond to last given"
}

case object InvalidUserId extends DomainValidation {
  override def errorMessage: String = "userId does not exist"
}

case object InvalidActionId extends DomainValidation {
  override def errorMessage: String = "actionId is not valid"
}

trait Validator {

  def validateEmail(email: String): ValidatedNel[DomainValidation, Unit] =
    if (EmailAddress.isValid(email)) ().validNel else InvalidEmail.invalidNel

  def validateAge(age: Int): ValidatedNel[DomainValidation, Unit] =
    if (age >= 5 && age <= 120) ().validNel else InvalidAge.invalidNel

  def validateGender(gender: Int): ValidatedNel[DomainValidation, Unit] =
    if (gender == 1 || gender == 2) ().validNel else InvalidGender.invalidNel

  def validateUser(user: User): ValidatedNel[DomainValidation, Unit] =
    validateEmail(user.email) |+| validateAge(user.age) |+| validateGender(user.gender)

  def validateAction(action: Action): ValidatedNel[DomainValidation, Unit] =
    if (Seq(1, 2, 3).contains(action.actionId)) ().validNel else InvalidActionId.invalidNel

}
