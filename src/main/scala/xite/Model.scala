package xite

object Model {

  case class User(userName: String, email: String, age: Int, gender: Int)
  case class Action(userId: Long, videoId: Long, actionId: Int)
  case class Recommendation(userId: Long, videoId: Long)

}
