package xite

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes

import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.pattern.ask
import akka.util.Timeout
import cats.data.Validated._
import xite.Model.{ Action, Recommendation, User }
import xite.UserRegistryActor.{ PerformAction, RegisterUser, RegistryMessage }

case class ErrorResponse(errors: Seq[String])

trait UserRoutes extends JsonSupport with Validator {

  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[UserRoutes])

  def userRegistryActor: ActorRef

  implicit lazy val timeout = Timeout(5.seconds)

  lazy val userRoutes: Route =
    path("register") {
      post {
        entity(as[User]) { user =>
          validateUser(user) match {
            case Valid(_) =>
              callRegistry(RegisterUser(user))
            case Invalid(errList) =>
              complete((StatusCodes.BadRequest, ErrorResponse(errList.map(_.errorMessage).toList)))
          }
        }
      }
    } ~
      path("action") {
        post {
          entity(as[Action]) { action =>
            validateAction(action) match { //not actually included in the task description, but playing it safe
              case Valid(_) =>
                callRegistry(PerformAction(action))
              case Invalid(errList) =>
                complete((StatusCodes.BadRequest, ErrorResponse(errList.map(_.errorMessage).toList)))
            }
          }
        }
      }

  private def callRegistry(msg: RegistryMessage) = {
    val resultF = (userRegistryActor ? msg).mapTo[Either[DomainValidation, Recommendation]]
    onSuccess(resultF) {
      case Left(e) => complete((StatusCodes.BadRequest, ErrorResponse(Seq(e.errorMessage))))
      case Right(r) => complete((StatusCodes.OK, r))
    }
  }
}
