package xite

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import xite.Model.{ Action, Recommendation, User }

trait JsonSupport extends SprayJsonSupport {

  import DefaultJsonProtocol._

  implicit val userFormat = jsonFormat4(User)
  implicit val actionFormat = jsonFormat3(Action)
  implicit val recommendationFormat = jsonFormat2(Recommendation)
  implicit val errorResponseFormat = jsonFormat1(ErrorResponse)

}
