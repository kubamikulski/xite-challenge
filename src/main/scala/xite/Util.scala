package xite

import scala.util.Random

object Util {

  def randomLongs: Stream[Long] = Stream.iterate(Random.nextLong)(_ => Random.nextLong).filter(_ > 0)

  val videoIds: Seq[Long] = randomLongs.take(10)

  val shuffledVideos: List[Long] = Random.shuffle(videoIds).toList

}
