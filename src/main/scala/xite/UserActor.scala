package xite

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import xite.Model.{ Recommendation, User }

object UserActor {
  final case class GetInitialRecommendation(origin: ActorRef)
  final case class GetNextRecommendation(origin: ActorRef, previousVideoId: Long)
  def props(user: User, id: Long): Props = Props(new UserActor(user, id))
}

class UserActor(user: User, id: Long) extends Actor with ActorLogging {
  import UserActor._

  def receive: Receive = awaitingFirstUse

  private def awaitingFirstUse: Receive = {
    case m: GetInitialRecommendation =>
      sendAndAwait(m.origin, Util.shuffledVideos)
  }

  private def issuedVideo(videoId: Long, remainingVideos: List[Long]): Receive = {
    case m: GetNextRecommendation if videoId == m.previousVideoId =>
      val recommendedList = if (remainingVideos.isEmpty) {
        log.info(s"user ${user.email} has watched all videos, rerolling new series")
        Util.shuffledVideos
      } else remainingVideos
      sendAndAwait(m.origin, recommendedList)
    case m: GetNextRecommendation =>
      m.origin ! Left(InvalidVideoId)
  }

  private def sendAndAwait(origin: ActorRef, recommendedList: List[Long]): Unit = {
    origin ! Right(Recommendation(id, recommendedList.head))
    context.become(issuedVideo(recommendedList.head, recommendedList.tail))
  }

}
